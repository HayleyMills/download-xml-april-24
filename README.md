## Export xml from archivist

*Note*: this is the step 2 from [create_new_export](https://gitlab.com/closer-cohorts1/archivist_export/create_new_export), i.e. no `get the latest version` step.


### Input
List of prefix: Prefixes_to_export.txt

### Output
- export_xml directory
    - xml_list.csv:
        - prefix
        - xml_date
        - xml_location
    - archivist_instance directory
        - raw xml files
    - archivist_instance_clean directory
        - cleaned xml files

### Data cleaning step

    - Clean text:
        - replace &amp;amp;# with &#
        - replace &amp;amp; with &amp;
        - replace &amp;# with &#
        - replace &#160: with &#160;
        - replace &#163< with &#163;<
        - replace &amp;amp;amp;# with &#

     - Remove text <duplicate [n]> from sequence labels

